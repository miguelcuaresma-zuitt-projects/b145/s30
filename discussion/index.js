// alert('hi')

// Javascript Synchronous and Asynchronous
	// single threaded
	// Javascript is by default is synchronous meaning that only one statement is executed at a time

// This can be proven when a statement has an error, javascript will not proceed with the next statement
console.log('Hello World');
// consle.log('Hello Again');
console.log('Goodbye');

// When certain statements take a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from databases
// This will take some time to process, this will result in code "blocking"
// We might not notice it due to the fast processing power of our devices
// example:
// for(let i = 0; i <= 1500; i++){
// 	console.log(i)
// };

// Only after the loop ends will this statement execute
console.log('loop is done')

// Asynchronous- we can proceed to execute other statements, while time consuming code is running in the background

// Getting all posts

// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
// Syntax
	// fetch('URL')

console.log(fetch('https://jsonplaceholder.typicode.com/posts'))



// Checking the status of the request

// Syntax
// fetch('URL')
// .then((response)=>{})

// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise

fetch('https://jsonplaceholder.typicode.com/posts')

// The "fetch" method will return a "promise" that resolves to a "Response" object
// The "then" method captures the "Reponse" object and returns another "promise" which will eventually be "resolved" or "rejected"
.then(response => console.log(response.status));

// Async and Await
// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for

async function fetchData(){

	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	// Result returned by fetch returns a promise
	console.log(result);

	// The returned "Response" is an object
	console.log(typeof result);

	// We cannot access the content of the "Response" by directly accessing it's body property
	console.log(result.body);

	// Converts the data from the "Response" object as JSON
	let json = await result.json();

	// Print out the content of the "Response" object
	console.log(json);
}

fetchData();

// Get a specific posts

// Retrieves a specific post following the Rest API (retrieve, /posts/:id, GET)
fetch('https://jsonplaceholder.typicode.com/posts/64')
.then((response) => response.json())
.then((json) => console.log(json))



// Create a post

// Syntax
	// fetch('URL', options)
	// .then((response)=>{})
	// .then((response)=>{})

// Creates a new post following the Rest API (create, /posts/:id, POST)

fetch('https://jsonplaceholder.typicode.com/posts', {

	// Sets the method of the "Request" object to "POST" following REST API
	// Default method is GET
	method: 'POST',

	// Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
	headers: {
		'Content-Type' : 'application/json'
	},

	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		title: 'New Post',
		body: 'I am a new post',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// Updating a post

// Updates a specific post following the Rest API (update, /posts/:id, PUT)

fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated Post',
		body: 'This is an updated post',
		userId: 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));



// Updating post using PATCH method

// Updates a specific post following the Rest API (update, /posts/:id, Patch)
	// The difference between PUT and PATCH is the number of properties being changed
	// PATCH is used to update the whole object
	// PUT is used to update a single/several properties

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Correct post'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Difference between PUT and POST
	// - The PUT is a method of modifying resource where the client sends data that updates the entire resource. It is used to set an entity’s information completely.
	// - The PATCH method applies a partial update to the resource.



// Delete a post
// Deleting a specific post following the Rest API (delete, /posts/:id, DELETE)
fetch('https://jsonplaceholder.typicode.com/posts/12', {
	method: 'DELETE'
});


// Filtering Posts

// The data can be filtered by sending the userId along with the URL
// Information sent vie the url can be done by adding the question mark symbol (?)
// Syntax
	// Individual Parameters
		// 'url?parameterName=value'
	// Multiple Parameters
		// 'url?paramA=valueA&paramB=valueB'
fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=10')
.then((response) => response.json())
.then((json) => console.log(json));


// Retrieving specific comments
// following the Rest API (retrieve, /posts/:id/comments, GET)
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));
