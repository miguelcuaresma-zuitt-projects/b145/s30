//5
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => { 
	let result = json.map(key => key.title);
	result.forEach(a => console.log(a));
});

//6
fetch("https://jsonplaceholder.typicode.com/todos?id=1")
.then((response) => response.json())
.then((json) => {
	console.log(json[0].title, json[0].completed)
})


//7.
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
	"userId": 1,
    "id": 101947,
    "title": "Fly",
    "completed": false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//8
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "this is some title",
		description: "some description",
		status: true,
		dateCompleted: "May 23, 2023",
		userId: 20220003
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

//10

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
    completed: "complete",
    dateStatusChanged: "26 January 2022"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/12", {
	method: "DELETE"
});